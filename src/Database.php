<?php

namespace Sendrill;

use Exception;

class Database
{
    public $db;

    private function is_float($string)
    {
        return is_numeric($string) && floor($string) != $string;
    }

    private function is_boolean($string)
    {
        return is_string($string) && (in_array(strtolower($string), ["true", "false"], true));
    }

    private function is_json($string)
    {
        if (!is_string($string)) return false;
        if (!is_array(json_decode($string, true))) return false;
        if (json_last_error() == JSON_ERROR_NONE) return true;
        return false;
    }

    private function ObjectifyTypes(&$val, $key = '')
    {
        if (is_int($val)) $val = (int)$val;
        if ($this->is_float($val)) $val = (float)$val;
        if ($this->is_json($val)) $val = json_decode($val, true);
        if ($this->is_boolean($val)) $val = (boolean)$val;
    }

    public function GetArray($qry, $params = [])
    {
        $stm = $this->db->prepare($qry);
        $stm->execute($params);
        $results = $stm->fetchAll(\PDO::FETCH_ASSOC);
        array_walk_recursive($results, [$this, 'ObjectifyTypes']);
        return $results;
    }

    public function GetObject($qry, $params = [])
    {
        $results = $this->GetArray($qry, $params);
        if (count($results) == 1 && isset($results[0])) $results = $results[0];
        return $results;
    }

    public function YieldArray($qry, $params = [])
    {
        $stm = $this->db->prepare($qry);
        $stm->execute($params);
        while ($data = $stm->fetch(\PDO::FETCH_ASSOC)) {
            array_walk_recursive($data, [$this, 'ObjectifyTypes']);
            yield $data;
        }
    }

    public function __construct($conf = [])
    {
        if (!extension_loaded('pdo')) throw new Exception('PDO is not loaded!');
        if ($conf == []) throw new Exception('No Conf specified');
        if (!isset($conf['db_port'])) $conf['db_port'] = '5432';
        if (!isset($conf['db_driver'])) $conf['db_driver'] = 'psql';
        if (!isset($conf['db_host'])) $conf['db_host'] = 'localhost';

        $ConnectionString = sprintf('%s:host=%s;dbname=%s;port=%s;', $conf['db_driver'], $conf['db_host'], $conf['db_database'], $conf['db_port']);
        $conn = new \PDO($ConnectionString, $conf['db_username'], $conf['db_password'], [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_EMULATE_PREPARES => true,
        ]);

        $this->db = $conn;
    }
}
